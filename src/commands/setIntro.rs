use std::str::FromStr;

use serenity::framework::standard::macros::command;
use serenity::framework::standard::{Args, CommandResult};
use serenity::model::prelude::*;
use serenity::prelude::*;
use serenity::model::channel::Message;
use serenity::utils::MessageBuilder;
use serenity::model::channel::ReactionType;

let intro_msg = 0;


async fn error_print(msg: &Message,ctx :&Context, message: String) -> CommandResult{
    let error_message = MessageBuilder::new().push("```").push(message).push("```").build();

        if let Err(why) = msg.channel_id.say(&ctx.http, error_message).await{
            println!("Error printing error message : {}", why);
        }  
        
        Ok(())
}

#[command]
pub async fn setIntro(ctx: &Context, msg: &Message, mut args: Args) -> CommandResult{
 
    // get the first args from the command !setIntro
    let mut result = match args.single::<String>() {
        Ok(text) => text,
        Err(_) => 
            String::from("")
    };

    let mut arg_vec = vec![result];

    let mut not_empty = true;

    //Store command args in arg_vec
    while not_empty{
        let res = match args.single::<String>() {
            Ok(text) => text,
            Err(why1) => 
                String::from("")
        };
        result = res;
        if result != "" {arg_vec.push(result)}
        else{ not_empty = false}
    }  

    arg_vec.reverse(); //reverse order of the args to treat arg in the right order

    //check the number of argument
    if arg_vec.len() < 2{
        error_print(msg, ctx, "Error ! usage : !setIntro [desired message] [dest chan ID] [[emoji] [target role ID]...]".to_string()).await?;
    }
 
    //get the desired content of the intro message
    let text_message = arg_vec.pop().expect("Fatal Error vector");

    println!("{text_message}");

    let s_channel_id = arg_vec.pop().expect("Fatal Error vector");

    println!("{s_channel_id}");
    let channel_id = match s_channel_id.parse() {
        Ok(e) => e,
        Err(e) => {println!("{e}"); 0}
    };

    //check that there is the same number of role than given reaction
    if arg_vec.len() % 2 != 0 {error_print(msg, ctx, "Odd number of emoji, role : \n Error ! usage : !setIntro [desired message] [dest chan ID] [[emoji] [target role ID]...]".to_string()).await?;}

    if channel_id == 0 {error_print(msg, ctx, "invalid channel ID".to_string()).await?;}

    let intro_text = MessageBuilder::new().push(text_message).build();

    let desired_chan = ctx.http.get_channel(channel_id).await?;

    let intro_message = desired_chan.id().say(&ctx.http, intro_text).await?;

    //react to the intro message
    while arg_vec.len() > 0{
        let emoji = ReactionType::from_str(&arg_vec.pop().expect("Fatal vector error"))?;
        
        let role_id_s = arg_vec.pop().expect("Fatal vector error");

        println!("role id :{role_id_s}");
        let role_id: u64 = match role_id_s.parse() {
            Ok(e) => e,
            Err(e) => 0
        };

        if role_id == 0 {
            error_print(msg, ctx, "invalid role ID".to_string()).await?;
        }

        intro_message.react(&ctx, emoji).await?;
    } 

    Ok(())

}