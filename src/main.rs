use std::env;


mod commands;

use serenity::{async_trait, prelude::EventHandler};
use serenity::prelude::*;
use serenity::model::channel::Message;
use serenity::framework::standard::macros::{command, group};
use serenity::framework::standard::{StandardFramework, CommandResult};

use crate::commands::setIntro::*;

#[group]
#[commands(ping, setIntro)]
struct General;

struct Handler;

#[async_trait]
impl EventHandler for Handler {
}

#[tokio::main]
async fn main() {
    let token = env::var("DISCORD_TOKEN").expect("discord token");

    let framework = StandardFramework::new().configure(|c| c.prefix("!")).group(&GENERAL_GROUP);

    let intents = GatewayIntents::non_privileged() | GatewayIntents::MESSAGE_CONTENT;
    let mut client = Client::builder(token, intents).event_handler(Handler).framework(framework).await.expect("Error creating client");

    if let Err(why) = client.start().await {
        println!("An error occurred while running the client: {:?}", why);
    }
}

#[command]
async fn ping(ctx: &Context, msg: &Message) -> CommandResult {
    msg.reply(ctx, "Pong!").await?;

    Ok(())
}
 